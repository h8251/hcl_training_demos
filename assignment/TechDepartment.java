package com.grateLearning.assignment;

class TechDepartment extends SuperDepartment{ 
	 
	 //declare method departmentName of return type string   
	 public String deparmentName() 
	 {  
		 return " Tech Department "; 
		 } 
	 //declare method getTodaysWork of return type string   
	 public String getTodaysWork()
	 {  
		 return "Complete coding of module 1 "; 
	 } 
	//declare method getTechStackInformation of return type string   
	 public String getTechStackInformation()
	 {  
		 return " core Java "; 
	 } 
	 //declare method getWorkDeadline of return type string 
	 public String getWorkDeadline() 
	 {  
		 return "Complete by EOD"; 
		 }
	 
	} 